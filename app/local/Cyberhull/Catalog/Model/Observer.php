<?php
/**
 * Cyberhull_Catalog Observer
 *
 * @package  Cyberhull_Catalog
 * @author: M.A.Borodov
 * @copyright: 2016 mborodov@gmail.com
 */
class Cyberhull_Catalog_Model_Observer {
    public function addColorFlag($observer) {

        // get collection from observer
        $collection = $observer->getCollection();

        // get only InStock products
        Mage::getSingleton('cataloginventory/stock')
            ->addInStockFilterToCollection($collection);

        // add color attribute for product collection
        $collection->addAttributeToSelect('color');
    }

}
