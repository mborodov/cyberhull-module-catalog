<?php
/**
 * Cyberhull_Catalog Helper
 *
 * @package  Cyberhull_Catalog
 * @author: M.A.Borodov
 * @copyright: 2016 mborodov@gmail.com
 */
class Cyberhull_Catalog_Helper_Data extends Mage_Core_Helper_Abstract {
    const COLOR_ATTRIBUTE = 'color';
    const CONFIG_PRODUCT_TYPE = 'configurable';

    /**
     * Check product has many colors
     */
    public function hasManyColors($_product) {
        $manyColors = false;

        // do only for configurable products
        if ($_product->getData('type_id') == self::CONFIG_PRODUCT_TYPE ) {

            //get the configurable data from the product
            $config = $_product->getTypeInstance(true);

            $attributes = $config->getConfigurableAttributesAsArray($_product);
            foreach ($attributes as $attribute) {
                $cCount = count($attribute["values"]);
                if ($attribute['attribute_code'] === self::COLOR_ATTRIBUTE && $cCount > 1) {
                    $manyColors = true;
                }
            }
        }
        
        return $manyColors;
    }

   
}
