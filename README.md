# README #

## Module for print many colors message.

![9b56f0fda5.jpg](https://bitbucket.org/repo/786gAe/images/3194297286-9b56f0fda5.jpg)

## How to use

Install module and add this code to list product phtml template


```
#!php
<?php
echo Mage::helper('cyberhull_catalog')->hasManyColors($_product) ? $this->__('This product is available in other colors') : ''?>
```